# Manual

## Description
Falcon is a data processing pipeline written in Java which consists of two services - [producer](https://gitlab.com/falcon-assessment/producer) and [consumer](https://gitlab.com/falcon-assessment/consumer).

The producer takes a valid JSON and sends it to a RabbitMQ exchange called producer.

The consumer binds to the producer exchange using a queue called consumer.
When a new message is received, it gets perisisted immediately in the database (Couchbase).
Finally, the clients have the ability to retrieve all of the produced messages.

## Cannonical source
The source of this manual is hosted [here](https://gitlab.com/falcon-assessment/manual).

## Requirements
In order to run the system, you will need the following already configured:

- Java 8 (or later)
- Git
- Maven
- Docker
- Docker Compose

## Setup Guide

#### Checkout
Checkout this project and then checkout both the producer and the consumer inside of it:
```
$ git clone https://gitlab.com/falcon-assessment/manual.git
$ cd manual/
$ git clone https://gitlab.com/falcon-assessment/producer.git
$ git clone https://gitlab.com/falcon-assessment/consumer.git
```

#### Hosts
For convenience we will add some hosts in order to access our different components easily later.


Append the following to your `/etc/hosts` file:
```
127.0.0.1       couchbase.com
127.0.0.1       producer.com
127.0.0.1       consumer.com
127.0.0.1       rabbitmq.com
```
###### IMPORTANT! This is used for local testing. Delete it when you are done.

#### Passwords
We need to set the passwords needed in order to connect to RabbitMQ and Couchbase for both the producer and the consumer.


###### Linux
Append the following to your `.bashrc` file:
```
# Set your own values if you want to
export RABBITMQ_PRODUCER_PASSWORD=producer
export RABBITMQ_CONSUMER_PASSWORD=consumer 
export BUCKET_JSON_DATA_PASSWORD=consumer
```
Open a new terminal tab and run the following to verify that the entries exist:
```
$ printenv
```

###### OS X
Run the following commands:
```
# Set your own values if you want to
$ launchctl setenv RABBITMQ_PRODUCER_PASSWORD producer
$ launchctl setenv RABBITMQ_CONSUMER_PASSWORD consumer 
$ launchctl setenv BUCKET_JSON_DATA_PASSWORD consumer
```
Restart the terminal and run the following to verify that the entries exist:
```
$ printenv
```


#### Build
Build the applications:
```
# Builds the jars needed for the docker build phase
$ /bin/sh package.sh
```
###### If the command fails, set `_JAVA_OPTIONS=-Djdk.net.URLClassPath.disableClassPathURLCheck=true` as an environment variable. [Learn more](https://stackoverflow.com/questions/53010200/maven-surefire-could-not-find-forkedbooter-class).

Build the docker images:
```
# Uses the docker-compose.yaml file in the current directory
$ docker-compose build
```

Run the system:
```
# Uses the docker-compose.yaml file in the current directory
$ docker-compose up -d
```
#### RabbitMQ setup
When the RabbitMQ container is up and running, do the following:
```
# Gets the rabbitmq's container id and opens it's shell (if there is an error, try without the $ )
$ docker exec -it $(docker ps -f name=rabbitmq --format "{{.ID}}") sh

# Creates a virtual host called pipeline
$ rabbitmqctl add_vhost pipeline

# Deletes the guest user. Not required.
$ rabbitmqctl delete_user guest

# Creates the admin user
$ rabbitmqctl add_user admin admin
$ rabbitmqctl set_user_tags admin administrator
$ rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
$ rabbitmqctl set_permissions -p pipeline admin ".*" ".*" ".*"


# Creates the producer users
# The last argument (password) must match the value of the RABBITMQ_PRODUCER_PASSWORD environment variable.
$ rabbitmqctl add_user producer producer

# Creates the producer users
# The last argument (password) must match the value of the RABBITMQ_CONSUMER_PASSWORD environment variable.
$ rabbitmqctl add_user consumer consumer

# Sets producer's permissions to the pipeline virtual host
$ rabbitmqctl set_permissions -p pipeline producer ".*" ".*" ".*"

# Sets consumer's permissions to the pipeline virtual host
$ rabbitmqctl set_permissions -p pipeline consumer ".*" ".*" ".*"

# Exits the shell
$ exit
```

#### Couchbase setup
When the couchbase container is up and running, do the following:
```
# Gets the couchbase's container id and opens it's shell (if there is an error, try without the $ )
$ docker exec -it $(docker ps -f name=couchbase --format "{{.ID}}") sh

# Inits a cluster with username admin and password 123456
$ couchbase-cli cluster-init -c 127.0.0.1 --cluster-username admin  --cluster-password 123456 --services data,query --cluster-ramsize 256

# Creates a bucket called json-data
$ couchbase-cli bucket-create -c 192.168.0.6:8091 --username admin --password 123456 --bucket json-data --bucket-type couchbase --bucket-ramsize 256

# Creates user called json-data with application access. The --rbac-password value must match the value of the BUCKET_JSON_DATA_PASSWORD environment variable.
$ couchbase-cli user-manage -c 127.0.0.1:8091 -u admin -p 123456 --set --rbac-username json-data --rbac-password consumer --roles bucket_full_access[json-data] --auth-domain local

# Creates a view called all that will allow the consumer to select all the documents from the bucket
$ curl -i -H "Content-Type: application/json;charset=UTF-8" -u admin:123456 -X PUT http://localhost:8092/json-data/_design/jsonData -d '{"views":{"all":{"map":"function (doc, meta) {\n emit(meta.id, null);\n}"}}}'

# Exits the shell
$ exit
```

## Usage

#### Producer
The producer consists of two key parts:
- `ProducerController` class with a single endpoint that consumes a valid JSON ( in case of invalid JSON "400 Bad Request" will be returned ).
- `MessageProducer` class with a single method that sends the consumed JSON to the RabbitMQ's producer exchange.

You can send a JSON to the producer in many different ways:
- Using any rest client
- Using the SwaggerUI at [http://producer.com/swagger-ui.html#/producer-controller`](http://producer.com/swagger-ui.html#/producer-controller)
- Using curl

###### Exaple using curl
```
# Sends test JSON to the producer
$ curl -i 'http://producer.com/json' -H 'Content-Type: application/json' -d '{"test":"test"}'
```

#### Consumer
The consumer consists of two key parts:
- `JsonMessageListener` class with a single method that persists the consumed JSON as a `RawJsonDocument` in the database.
- `ConsumerController` class with a single endpoint that returns all of the persisted documents from the database.

You can get all of the documents in many different ways:
- Using any rest client
- Using curl

###### Example using curl
```
# Gets the persisted documents from the database
$ curl http://consumer.com/json/all
```
## Reminder
When you are done, do the following:
```
# Uses the docker-compose.yaml file in the current directory
$ docker-compose down
```
Remove the following entries from your `/etc/hosts` file:
```
127.0.0.1       couchbase.com
127.0.0.1       producer.com
127.0.0.1       consumer.com
127.0.0.1       rabbitmq.com
```
